from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, TodoItemForm
# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()

    context = {
        'todo_object': todo,
    }
    return render(request, 'todos/list.html', context)



def todo_list_detail(request, id):
    list_detail = TodoList.objects.get(id=id)
    items=TodoItem.objects.filter(list=list_detail)
    context = {
        'todo_object': list_detail,
        'items': items,
    }
    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = ListForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=post)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = ListForm(instance=post)

    context = {
        "form": form
        }

    return render(request, "todos/edit.html", context)



def todo_list_delete(request, id):
  post = TodoList.objects.get(id=id)
  if request.method == "POST":
    post.delete()
    return redirect('todo_list_list')

  return render(request, "todos/delete.html")


def todo_item_create(request):
  if request.method == "POST":
    form = TodoItemForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
        items= form.save()
        return redirect("todo_list_detail", id=items.list.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoItemForm()

  context = {
    "form": form
  }

  return render(request, "todos/items_create.html", context)



def todo_item_update(request, id):
  post = TodoItem.objects.get(id=id)
  if request.method == "POST":
    form = TodoItemForm(request.POST, instance=post)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      items = form.save()
      return redirect("todo_list_update", id=items.id)

      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
  else:
    form = TodoItemForm(instance=post)

  context = {
    "form": form
  }

  return render(request, "todos/update.html", context)
